var mysql= require('mysql');
var AWS = require('aws-sdk');
var EventEmitter = require("events").EventEmitter;
var util = require('util');

exports.handler = function(event, context) {
    console.log(util.inspect(event, false, null));
    var recordsToProcess = 0;
    var mysqlRecords = 0;
    var dynamoRecords = 0;
    var ee = new EventEmitter();
    var table = new AWS.DynamoDB({params: {TableName: 'cis-655-2015-fall'}});
    var mysqlConn = mysql.createConnection({
      host     : 'cis-655-mysql.chslzpar1plu.us-east-1.rds.amazonaws.com',
      user     : 'cis655',
      password : 'cis655mysql',
      database : 'cis655',
      multipleStatements : true
    });

    mysqlConn.connect();

    event.Records.forEach(function(record) {
        item = record.dynamodb.NewImage;
        mysqlConn.query('INSERT INTO tracking SET ?', { 
            email: item.email.S, button: item.button.S , event: record.eventName
        }, function (err, result) {
            mysqlRecords += 1;
            console.log('mysql updated:', result, err);
            ee.emit('mysqlUpdated', item.email)
        });
    });

    ee.on("mysqlUpdated", function (email) {
        if(mysqlRecords == event.Records.length) {
            mysqlConn.end();
            context.succeed("Successfully processed " + mysqlRecords + " records.");
        }
    });
};