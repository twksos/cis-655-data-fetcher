var express = require('express')
var app = express()
var AWS = require('aws-sdk');
var mysql= require('mysql');

AWS.config.region = 'us-east-1';

app.get('/', function (req, res) {

    var table = new AWS.DynamoDB({params: {TableName: 'cis-655-2015-fall'}});

    var mysqlConn = mysql.createConnection({
      host     : 'cis-655-mysql.chslzpar1plu.us-east-1.rds.amazonaws.com',
      user     : 'cis655',
      password : 'cis655mysql',
      database : 'cis655'
    });
    mysqlConn.connect();

    var params = {
        TableName: 'cis-655-2015-fall',
        FilterExpression: 'exported = :exported',
        ExpressionAttributeValues: {
            ':exported': { BOOL:false },
        }

    };
    table.scan(params, function(err, data){
        if (err) {
            console.log(err, err.stack);
            res.send({'error': err});
        } 
        else {
            for (var item in data.Items) {
                item = data.Items[item];
                console.log(item);
                mysqlConn.query('INSERT INTO tracking SET ?', { 
                    email: item.email.S, button: item.button.S 
                }, function (err, result) {
                    table.updateItem({
                        Key: {
                            email: item.email
                        },
                        UpdateExpression: 'SET exported = :exported',
                        ExpressionAttributeValues: {
                            ':exported': { BOOL:true },
                        }
                    }, function(err, data) {
                        if (err) console.log(err, err.stack); // an error occurred
                        else     console.log(data);           // successful response
                    })
                })
            }
        }
    })
    res.send('{success:200}');
})
 
app.listen(80)