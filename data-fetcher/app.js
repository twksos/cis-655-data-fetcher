var express = require('express')
var app = express()
var url = require('url');
var AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';

app.get('/', function (req, res) {

    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;

    var key = query.email;
    var data = query.button;

    var table = new AWS.DynamoDB({params: {TableName: 'cis-655-2015-fall'}});
    var itemParams = {Item: {email: {S: key}, button: {S: data}, exported: {BOOL: false}}};
    table.putItem(itemParams, function(err, data) {
        if (err) {
            console.log(err, err.stack);
            res.send({'error': err});
        } 
        else res.send({'success': key});
    });

})
 
app.listen(80)